﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp.WindowsDesktop.Models
{
    internal class Product
    {
        #region Properties
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public bool isAvailable { get; set; }
        #endregion

        public override string ToString()
        {
            return $"Id::{Id}\tName::{Name}\tDesciption::{Description}\tAvailiblity::{isAvailable}";
        }
    }

}
